<h1>Okta Backend</h1>

Aplicação de exemplo para um backend java com spring provendo validação de token via okta.

## Run 

```
$ mvn package
$ mvn spring-boot:run

Acessar o swagger em http://localhost:8080/swagger-ui.html
```