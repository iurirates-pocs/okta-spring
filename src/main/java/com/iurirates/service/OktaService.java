package com.iurirates.service;

import com.okta.jwt.AccessTokenVerifier;
import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerifiers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Duration;
import java.time.Instant;

@Service
public class OktaService {

    private com.okta.authn.sdk.AuthenticationStateHandler authenticationStateHandler;

    AccessTokenVerifier jwtVerifier = JwtVerifiers.accessTokenVerifierBuilder()
            .setIssuer("https://dev-30006482.okta.com/oauth2/default")
            .setAudience("0oa13yio7ol7mN5Yy5d7")
            .setConnectionTimeout(Duration.ofSeconds(1))
            .setRetryMaxAttempts(2)
            .setRetryMaxElapsed(Duration.ofSeconds(10))
            .build();

    public ResponseEntity<?> validar(String token){
        try{
            Jwt jwt = jwtVerifier.decode(token);
            if (jwt != null && Instant.now().isBefore(jwt.getExpiresAt())){
                return ResponseEntity.status(HttpStatus.OK).body("Token Válido até: " + Date.from(jwt.getExpiresAt()));
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }
}
