package com.iurirates.controller;

import com.iurirates.service.OktaService;
import com.okta.jwt.JwtVerificationException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OktaController {

    @Autowired
    private OktaService oktaService;

    @ApiOperation(notes = "Validar Token", value = "Realiza a validação do token ", nickname = "validar")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @GetMapping(path="/auth/validar/{token}")
    public ResponseEntity<?> validar(@PathVariable String token) throws JwtVerificationException {
        return oktaService.validar(token);
    }

}
